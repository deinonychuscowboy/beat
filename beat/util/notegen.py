for x in range(0,128):
	letter=[]
	if x%12==0:
		letter=["C"]
	elif x%12==1:
		letter=["CS","DF"]
	elif x%12==2:
		letter=["D"]
	elif x%12==3:
		letter=["DS","EF"]
	elif x%12==4:
		letter=["E"]
	elif x%12==5:
		letter=["F"]
	elif x%12==6:
		letter=["FS","GF"]
	elif x%12==7:
		letter=["G"]
	elif x%12==8:
		letter=["GS","AF"]
	elif x%12==9:
		letter=["A"]
	elif x%12==10:
		letter=["AS","BF"]
	elif x%12==11:
		letter=["B"]

	val=int(x/12)-1
	if val<0:
		val="N1"

	for l in letter:
		print(l+str(val)+"="+str(x))