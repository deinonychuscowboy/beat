S64=127<<8
S64_128=126<<8
S32=125<<8
S32_64=124<<8
S16=123<<8
S16_32=122<<8
S8=121<<8
S8_16=120<<8
S4=119<<8
S4_8=118<<8
S2=117<<8
S2_4=116<<8
L1_05=115<<8
L2=114<<8
L2_1=113<<8
L4=112<<8
L4_2=111<<8
L8=110<<8
L8_4=109<<8
L16=108<<8
L16_8=107<<8
L32=106<<8
L32_16=105<<8
L64=104<<8

def compute_length(value):
	if value==0:
		return 1.0
	if value==S64:
		return 1./64.
	if value==S64_128:
		return 1./64.+1./128.
	if value==S32:
		return 1./32.
	if value==S32_64:
		return 1./32.+1./64.
	if value==S16:
		return 1./16.
	if value==S16_32:
		return 1./16.+1./32.
	if value==S8:
		return 1./8.
	if value==S8_16:
		return 1./8.+1./16.
	if value==S4:
		return 0.25
	if value==S4_8:
		return 0.25+1./8.
	if value==S2:
		return 0.5
	if value==S2_4:
		return 0.75
	if value==L1_05:
		return 1.5
	if value==L2:
		return 2.
	if value==L2_1:
		return 3.
	if value==L4:
		return 4.
	if value==L4_2:
		return 6.
	if value==L8:
		return 8.
	if value==L8_4:
		return 12.
	if value==L16:
		return 16.
	if value==L16_8:
		return 24.
	if value==L32:
		return 32.
	if value==L32_16:
		return 48.
	if value==L64:
		return 64.