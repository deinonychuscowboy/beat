
def C(root):
	return root, root+4, root+7

def CM6(root):
	return root, root+4, root+7, root+9

def C7(root):
	return root, root+4, root+7, root+10

def CM7(root):
	return root, root+4, root+7, root+11

def Ca(root):
	return root, root+4, root+8

def Ca7(root):
	return root, root+4, root+8, root+10

def Cm(root):
	return root, root+3, root+7

def Cm6(root):
	return root, root+3, root+7, root+9

def Cm7(root):
	return root, root+3, root+7, root+10

def CmM7(root):
	return root, root+3, root+7, root+11

def Cd(root):
	return root, root+3, root+6

def Cd7(root):
	return root, root+3, root+6, root+9

def Chd7(root):
	return root, root+3, root+6, root+10

def Cr(root): # rotated major chord (intervals 6, 5, 4 instead of 5, 4, 6 as in regular C
	return root, root+5, root+9

def CM6r(root):
	return root, root+2, root+5, root+9

def C7r(root):
	return root, root+3, root+5, root+9

def CM7r(root):
	return root, root+4, root+5, root+9

# no rotations of augumented chords since they are equidistant

def Ca7r(root):
	return root, root+2, root+4, root+8

def Cmr(root):
	return root, root+5, root+8

def Cm6r(root):
	return root, root+2, root+5, root+8

def Cm7r(root):
	return root, root+3, root+5, root+8

def CmM7r(root):
	return root, root+4, root+5, root+8

def Cdr(root):
	return root, root+6, root+9

# cd7 is equidistant

def Chd7r(root):
	return root, root+4, root+6, root+9

def Crr(root): # double-rotated major chord
	return root, root+3, root+8

def CM6rr(root):
	return root, root+3, root+5, root+8

def C7rr(root):
	return root, root+3, root+6, root+8

def CM7rr(root):
	return root, root+3, root+7, root+8

# equidistant

def Ca7rr(root):
	return root, root+4, root+6, root+8

def Cmrr(root):
	return root, root+4, root+9

def Cm6rr(root):
	return root, root+4, root+6, root+9

def Cm7rr(root):
	return root, root+4, root+7, root+9

def CmM7rr(root):
	return root, root+4, root+8, root+9

def Cdrr(root):
	return root, root+3, root+9

# equidistant

def Chd7rr(root):
	return root, root+3, root+7, root+9

# TODO these are only "common chords" according to wikipedia and do not include a lot of the other numbers, look at the list of symbols on wikipedia. add is handled by interval functions and tuple addition. Omit can be handled by using dyads instead
