#!/usr/bin/env python3
import mido, sys, tempfile, time, os
from . import gm, l
from .n import N

notepattern=[]
current_program=0
next_program=0
channel=0
tpb=480
bmu=4
bml=1
current_bpm=0
next_bpm=120
port_in=None
port_out=None
continuant=False

def render(tracks):
	fname=None
	if port_out is None:
		fname=sys.argv[2]
	else:
		fname=tempfile.mkstemp()[1]

	file=create_file(fname,tracks)

	if port_out is not None:
		for msg in file:
			time.sleep(msg.time)
			if not msg.is_meta:
				port_out.send(msg)
	time.sleep(5)

def set_recurrent_pattern():
	global continuant
	continuant=False

def set_continuant_pattern():
	global continuant
	continuant=True

def set_port(name,outname=None):
	global port_in
	global port_out
	if sys.argv[1]=="port":
		for x in mido.get_input_names():
			if name in x:
				port_in=mido.open_input(x)
				break
		if outname is None:
			outname=name
		for x in mido.get_output_names():
			if outname in x:
				port_out=mido.open_output(x)
				break
		if port_in is None:
			raise Exception("Unable to connect to MIDI port: "+name)

def create_file(fname,tracks):
	file=mido.MidiFile()
	file.ticks_per_beat = tpb
	for x in tracks:
		file.tracks.append(x)
	os.makedirs(os.path.dirname(fname),exist_ok=True)
	file.save(fname)
	return file

def set_unit_pattern(pattern):
	"""
	Establishes the base pattern used to supply notes to the rhythm. If fewer notes are provided than the total number of notes in a unit, the last note is repeated.
	"""
	global notepattern
	notepattern=pattern

def set_program(program):
	"""
	Sets the midi program to use for subsequent commands, including supporting bank selection (byte shifted; upper 256 bits are coarse bank, middle 256 are fine bank, lower 256 are program number)
	"""
	global next_program
	next_program=program

def set_channel(ch):
	"""
	Sets the midi channel to use for subsequent commands
	"""
	global channel
	channel=ch-1

def set_ticks_per_beat(t):
	"""
	Sets the number of midi ticks per beat; this should be synchronized with the value in the midi file
	"""
	global tpb
	tpb=t

def set_beats_per_unit(b):
	"""
	Sets the number of beats per unit. A unit length should be equal to or smaller than a measure length, e.g. if you have 6 beats per measure with 4 beats per unit, your measure length will be 1u,2b. If the unit length is equal to the measure length, the unit pattern will span the entire measure; if it is smaller, the unit pattern will repeat more than once throughout a measure.
	"""
	global bmu
	bmu=b

def set_units_per_measure(b):
	"""
	Sets the number of units per measure. This times the beats per unit is equivalent to the top number of a time signature.
	"""
	global bml
	bml=b

def set_beats_per_minute(b):
	"""
	Sets the number of beats per minute. This establishes the overall tempo of the midi sequence.
	"""
	global next_bpm
	next_bpm=b

def hit(sound,length):
	"""
	Output a midi message for this type of note and duration
	"""
	global current_program
	global current_bpm
	messageset=[]
	if next_program!=current_program:
		messageset.append(mido.Message("control_change", control=0, value=((255<<16)&next_program)>>16))
		messageset.append(mido.Message("control_change", control=32, value=((255<<8)&next_program)>>8))
		messageset.append(mido.Message("program_change", program=(255&next_program)))
		current_program=next_program
	if next_bpm!=current_bpm:
		messageset.append(mido.MetaMessage("set_tempo", tempo=mido.bpm2tempo(next_bpm)))
		current_bpm=next_bpm

	max_len=0
	if type(sound) is tuple or 255&sound!=N:
		if type(sound)!=tuple:
			sound=(sound,)
		first=True
		for x in sound:
			lengthvalue=l.compute_length(127<<8&x)
			if lengthvalue>max_len:
				max_len=lengthvalue
			notevalue=127&x
			messageset.append(mido.Message('note_on',channel=channel,note=notevalue,time=1 if first else 0,velocity=0 if 255&x==N else 64)) # TODO support velocity? Maybe we don't care
			first=False
	else:
		max_len=l.compute_length(127<<8&sound)
		messageset.append(mido.Message('note_on',channel=channel,note=0,time=1,velocity=0))
	max_len*=length
	return tuple(messageset), max_len

def unit(length,subdivision,hit_num):
	"""
	take a length, a number of beats to subdivide it into, and an optional initial time offset, and output a unit sequence of that length containing those notes
	"""
	global notepattern
	if len(notepattern)==0:
		notepattern=[gm.chan10.ACOUSTIC_SNARE,gm.chan10.SIDE_STICK]
	noteset=[]
	first=True
	basis=0
	if continuant:
		basis=int(hit_num)
	usage=0
	counter=0
	durations=[]
	while usage<subdivision:
		# this returns the duration of a note, MIDI needs the time of a note. Time is equal to the duration of the previous note (0 for the first note), within a track, for our purposes.
		hits, duration=hit(notepattern[min(len(notepattern)-1,counter+basis)],int(length/subdivision))
		for y in hits:
			noteset.append(y)
		durations.append(duration)
		usage+=duration/(length/subdivision)
		counter+=1
	return tuple(noteset), tuple(durations)

def measure(spec):
	"""
	combine multiple unit specs into a recurring pattern over a measure length, taking an optional deferred time offset if the previous measure did not fill its entire allotted time
	"""
	val=[]
	ml=bml*bmu
	deferred=ml
	for x in spec:
		val.append(float(x[:-1]) * bmu if x[-1] == "u" else float(x[:-1]) if x[-1] == "b" else -1)
		deferred-=(bmu if x[-1] == "u" else 1 if x[-1] == "b" else 100000)
	deferred*=tpb
	total=sum(val)
	unitset=[]
	durationset=[]
	aggregate=0
	for i in range(0,len(val)):
		units,durations=unit(int(tpb*ml*val[i]/total),val[i],aggregate)
		aggregate+=val[i]
		for y in units:
			unitset.append(y)
		for y in durations:
			durationset.append(y)
	if deferred>0:
		unitset.append(mido.Message('note_on',channel=channel,note=0,time=1,velocity=0))
		durationset.append(deferred)
	return tuple(unitset),tuple(durationset)

def repeat(spec,count):
	"""
	repeat a pattern a certain number of times
	"""
	patset=[]
	durset=[]
	for x in range(0, count):
		pat,dur=measure(spec)
		for y in pat:
			patset.append(y)
		for y in dur:
			durset.append(y)
	return tuple(patset), tuple(durset)

def rhythm(*spec,**options):
	"""
	Build a midi track from a series of repeat commands (alternating measure specs with repeat counts), including options for the entire track

	Unit spec: specifies how many notes to output in a small chunk, using the unit pattern to determine what notes to output. This is a number followed by a letter, with the letter being either b or u. b indicates that the number is the number of beats to output. u indicates that the number is the number of units to output, of the default unit length. So, if the unit length is 4, and you pass 1u, you will get 4 notes. If you pass 2b, you will get 2 notes at half tempo. If you pass 2u, you will get 8 notes at double tempo.
	Measure spec: a sequence of unit specs separated by commas, which should equal the total beats per measure. So, if the beats per measure is 6 and the beats per unit is 4, your measure spec should be something like 1u,2b, or 2u,4b for double tempo.
	"""
	track = mido.MidiTrack()
	if "pattern" in options:
		set_unit_pattern(options["pattern"])
	if "program" in options:
		set_program(options["program"])
	if "channel" in options:
		set_channel(options["channel"])
	else:
		set_channel(10)
	if "continuant" in options and options["continuant"]:
		set_continuant_pattern()
	else:
		set_recurrent_pattern()
	spec=zip(*(iter(spec),) * 2)
	full_pat=[]
	full_dur=[]
	for s in spec:
		pat=s[0].split(",")
		genpat,gendur=repeat(pat,s[1])
		counter=0
		for message in genpat:
			full_pat.append(message)
		for dur in gendur:
			full_dur.append(dur)
	first=True
	counter=0
	for message in full_pat:
		if message.type=="note_on":
			if first:
				message.time=0
				first=False
			elif message.time==1: # needs a time assignment
				message.time=int(full_dur[counter])
				counter+=1
		track.append(message)
	assert counter==len(full_dur)-1,"Should have only one remaining duration for the last note"
	return track

def notes(*spec,**options):
	track = mido.MidiTrack()
	if "program" in options:
		set_program(options["program"])
	if "channel" in options:
		set_channel(options["channel"])
	if "recurrent" in options and options["recurrent"]:
		set_recurrent_pattern()
	else:
		set_continuant_pattern()
	if len(spec)>=2 and type(spec[1])==str:
		spec=zip(*(iter(spec),) * 2)
	else:
		spec=tuple((x,) for x in spec)
	full_pat=[]
	full_dur=[]
	for s in spec:
		patset=[]
		if len(s)==1:
			units=int(len(s[0])/bmu)
			rem=int(len(s[0])%bmu)
			patset.append(str(units+(1 if rem>0 else 0))+"u")
			s=(s[0]+(N,),)
		else:
			patset=s[1].split(",")
		set_unit_pattern(s[0])
		genpat,gendur=repeat(patset,1)
		for message in genpat:
			full_pat.append(message)
		for dur in gendur:
			full_dur.append(dur)
	first=True
	counter=0
	for message in full_pat:
		if message.type=="note_on":
			if first:
				message.time=0
				first=False
			elif message.time==1: # needs a time assignment
				message.time=int(full_dur[counter])
				counter+=1
		track.append(message)
	assert counter==len(full_dur)-1,"Should have only one remaining duration for the last note"
	return track
