# Some Starting Documentation

Beat is a midi sequencer written in python, on top of the excellent library mido. You'll need to set up mido first to use Beat.

Beat abstracts away the technicalities of the MIDI format, and presents an API which is somewhere between musical notation (which there are already plenty of MIDI format abstractions for) and standard programming API. "Writing music for programmers."

Beat includes its core library, beat.py, as well as a number of other libraries that mostly contain constants for use in notation.

c.py -- chords

d.py -- dyads

i.py -- intervals

l.py -- note length variations

n.py -- notes

Beat was originally written primarily for rhythm tracks, and so it relies on the principle of the regular unit.

A midi file (song, etc.) is composed of tracks. Tracks are a mido feature. A track is composed of repeats, or blocks of many notes that are repeats of a single measure. A repeat is then composed of measures, each one of which is identical within the repeat, but of course different repeats can have different measures, and many repeats may only have one measure (i.e. not repeated). Finally, a measure is composed of units. The fundamental expectation used in beat is that most notes will be regular within a unit; that is to say, with 4/4 time, a steady rhythm might have 4 beats, or 8 beats, but all equidistant. Never fear -- this is the default expectation, not the requirement.

In musical terms, this works out to the following: Assuming a simple case like 4/4 time and one unit per measure, Beat will assume, by default, that every note is a quarter note. If you want a note to not be a quarter note, you annotate it with length, which we'll get to later, but quarter notes require no annotation. If you instead have 4/4 time and 2 units per measure, every note is an eighth note, etc. Beat is also quite capable of handling more interesting time signatures and uneven unit/measure relationships, but always relies on the assumption that within a unit, there is a "default note length" that is predictable and all other lengths operate in relation to that.

In practical terms, this means that some elements of notation which are obvious in sheet music are more obsure in Beat. It's hard to look at a note and know exactly how long it will sound. It is, however, easy to look at a measure and know how its notes will be organized internally. It is also more flexible regarding rhythmic changes and interesting rhythms, and less work for very consistent pacing like rhythm tracks, which can be specified very simply.

# Staring a song

Songs in Beat are just python files.

```
from beat import *
from beat.n import *
from beat.d import *
from beat.c import *
from beat.l import *
from beat import i,gm,mm68
```
Here we import the beat standard utilities, the notes, the dyads, the chords, and the note lengths. These constants are all designed not to overlap for the most part, but since dyads and intervals generally use the same names, you can see we imported intervals with a non-from import, since we may be using dyads more often than intervals. We also import the general midi programs, channel 10 percussion, and bank programs for my own synth, the Yamaha MM6/8 series. Beat supports extended banks and it's easy to write your own synth's constants; I did mine in about 5 minutes in Excel from the digital manual's table. MIDI uses two values for the bank selection, a high value and a low value, as well as the normal program number. In Beat, these are just byte shifted integers -- shifted two bytes left for the high bank value, one byte for the low bank value, and unshifted for the program number.

```
set_port("MM6")
set_ticks_per_beat(480)
set_beats_per_unit(4)
set_units_per_measure(1)
set_beats_per_minute(120)
```

Now, some formalities. We set the midi port to output to, here my synth over USB. Beat will search for a mido port containing the substring you specify when calling the set_port method, and connect to that, so there's no need to look up the port number every time as long as your synth or system midi interface has a consistent name (if it doesn't, you can also just save a midi file by passing a filename when launching beat).

We set the ticks per beat to 480. This is an internal MIDI value; 480 is mido's default and should be enough for anyone. Low numbers might cause issues with very short notes, high numbers might cause issues with your system's clock not being accurate enough.

We set beats per unit to 4. Note here that "beat" and "note" are not the same thing; as noted before, with a basic case, you can imagine beats are quarter notes, even if your unit has eighth notes in it.

We set units per measure to 1. Beats per unit times units per measure equals the top number of a time signature.

We set beats per minute to 120. This controls the overall pacing of the song, as you might expect.

# Writing notes

Now Beat is ready to generate us some midi messages. You probably want to begin with an array of tracks, as you will likely have multiple; every track starts at the beginning of the song and proceeds for the duration of its notes, as you might expect. Tracks can contain concurrent notes in limited cases, like chords, but overlapping notes should really be handled with separate tracks.

```
tracks=[]
tracks.append(
	notes(
		(C4,D4,E4,F4),
    )
)
```

Here, we use the note notation provided by n.py to identify our notes with two-character sequences that are recognizable to a musician (rather than the internal MIDI codes, which are just numbers). The `notes()` method takes any number of tuples, each tuple being a unit, and intuits the proper pacing for the notes within them. Since we have 4 notes, our default beats per unit of 4 means that these will be quarter notes. This is called a "1u unit", or a unit in single pacing.

```
tracks.append(
	notes(
		(C4,D4,E4,F4,G4,A4,B4,C5),
    )
)
```

Now, since we have 8 notes, and our beats_per_unit is 4, the notes method will intuit that we mean these notes to be eighth notes; this is called a "2u unit", or a unit in double pacing.

You can also specify unit length manually.

```
	notes(
		(C4,D4,E4,F4),"2u"
    )
)
```

Here we have 4 notes in double pacing, so we'll get 4 eighth notes, followed by a half rest. If you want to insert rests manually earlier in a unit, the note code for a rest is N.

```
	notes(
		(C4,N,D4,N,E4,N,F4),"2u"
    )
```

A total of 7 eighth notes/rests, followed by an auto-generated eighth rest.

Unit pacing can also be specified in terms of beats, if you need more fine-grained control for a more complex rhythm.

```
	notes(
		(C4,D4,E4,F4),"3b"
    )
```

Here we specify that the entire unit will only have 3 beats. Note, that this means the final note will never be played! Likewise, if you have a more complex unit/measure relationship, such as a beats per unit of 4 and a units per measure of 1.25 for 5/4 time, your 1u and 2u units will not take up the entire measure, and will have a rest at the end, unless you follow them with a 1b unit.

If you specify your unit pacing for one unit, you must do so for all units in the `notes()` call. However, it's easy to mix units that do and do not have manual pacing; just add them together.

```
tracks.append(
	notes(
		(C4,D4,E4,F4),"3b"
    )+
	notes(
		(C4,D4,E4,F4),
    )
)
```

# Note lengths

To change the default assumption about how long notes should be within a unit, you can use note length annotations, from l.py. These are ORed with each note, and indicate subdivisions or multiplications of the note. So, if you do

```
	notes(
		(C4|S2,D4|S2,E4|S2,F4|S2),
    )
```

That is, subdivide all the notes by 2, you'll get 4 eighth notes, even though the unit would otherwise be calculated to have four quarter notes. This system permits you to use variable notes, though other features of Beat like unit pacing involve less typing.

```
	notes(
	    (C4,D4|S2,E4,F4|S2,G4,A4|S2,B4|L2_1,C5,D5,E5,F5,G5,A5,B5,C6),"2u"
    )
```

L2_1 is a lengthening by 2 and 1, or double-plus-one length, or three of the base unit notes. If the beats per unit is 4, this is a dotted half note. If you count out the note values here, you'll see that only the first scale gets played, the second is skipped.

As acutal note values are fully abstracted by Beat, all the note length annotations are relative and represent fractional values. So, you have S2, S4, S8 etc for one half, one fourth, and one eighth of the default length, and S2_4, S4_8, etc. for dotted versions of those. On the other side, you have L2, L4, etc. for double, quadruple the length, and L1_05, L2_1, L4_2 for dotted single, double, and quadruple lengths.

# Intervals, Dyads, and Chords

Beat provides a fast way to compute intervals with i.py:

```
    notes(
        (i.P4(C4),),
    )
```

The P4 interval of C4 is F4. Intervals by themselves are not super useful, but become much more useful interpreted as dyads, and in combination with chords.

```
    notes(
        (P4(C4),),
    )
```

Here we compute the P4 dyad (assuming, as above, that you did `from d import *` and `import i`) based only on the root note, which is always the lowest note in a harmonic. If you prefer, you can also combine intervals, dyads, and chords with simple math (but note any directly specified notes must be tuples):

```
    notes(
        ((C4,)+i.P4(C4),),
    )
```

This also gets you the P4 dyad.

Chords work similarly, using standard chord notation.


```
    notes(
        (CM7(C4),),
    )
```

Here we see the C major 7 chord, which has 4 notes. c.py contains most common chord notations, and for anything more obscure/jazzy, you can build your own with intervals.


```
    notes(
        (Cd(C4)+i.M7(C4), P5(C4)+i.P4(C4)+i.M7(C4),),
    )
```

These are the Cdim add7 and Csus4 add7 chords.

# A few more notes

To wrap up the `notes()` method, there are a few optional parameters you can pass to affect your notes.


```
    notes(
        (CM7(C4),),
        channel=3,
        program=gm.prog.COOL_GALAXY_EP,
        recurrent=True
    )
```

Here we place the notes into MIDI channel 3, specify what program to use to play them, and specify a recurrent pattern. By default, `notes()` calls are continuant, meaning each unit within a measure starts in the note pattern where the previous one left off. Recurrent patterns start over with each unit. This will make more sense in the next section, where the default is the reverse.

# Rhythm tracks

Rhythm tracks are very similar in nature to notes tracks, but exploit a few more abstractions to make them even simpler to write.

```
tracks=[]
tracks.append(
    rhythm(
        "1u",16,
    )
)
```

Unlike `notes()`, the bread and butter of a call to `rhythm()` is not the note specification. Rather, you merely provide a measure specification (here a simple 1u unit) and a number of times to repeat it (16). For most rhythm purposes, if you're okay with general midi, you you will want to be on channel 10 (the default for `rhythm()`, as this channel is always used for percussion in the general midi standard. Your synthesizer may offer better options through programs, however, in which case you may want to pass the channel and program options just as you would with `notes()`.

The above gives you a default rhythm, but of course you likely want to use your own. That's done with the pattern option. This option works like the note specifications we saw before, but since notes are less important for rythms, it just gets passed as an option and reused.

```
tracks.append(
    rhythm(
        "2u",16,
        pattern=[gm.chan10.TAMBOURINE]
    )
)
```

As noted above, the length in the `rhythm()` call is not a unit length (like in `notes()`), but a measure length specification. What's the difference? Well, if you want to use a more complex measure-to-unit relationship:

```

set_beats_per_unit(4)
set_units_per_measure(1.25)

tracks=[]
tracks.append(
    rhythm(
        "1u,1b",16,
        pattern=[gm.chan10.ACOUSTIC_SNARE,gm.chan10.SIDE_STICK,N,gm.chan10.SIDE_STICK]
    )
)

```

Here we have two units in 5/4 time, the first being four beats long, the second 1 beat. You can also vary this up per measure.

```

set_beats_per_unit(4)
set_units_per_measure(1.25)

tracks=[]
tracks.append(
    rhythm(
        "1u,1b",4,
        "3b,2b",4,
        "1u",4
        pattern=[gm.chan10.ACOUSTIC_SNARE,gm.chan10.SIDE_STICK,N,gm.chan10.SIDE_STICK]
    )
)

```

The last measure will have a rest at the end, just like with notes, because it didn't fill the entire measure time. Note that above, the recurrent behavior of the `rhythm()` method becomes apparent -- the pattern given starts over with every new unit. If you want to instead use continuant mode, you can specify that just like with `notes()`. Note that if you do not supply enough notes in the pattern argument, the final note will be repeated.

```
    rhythm(
        "3b,2b",4,
        pattern=[gm.chan10.ACOUSTIC_SNARE,gm.chan10.SIDE_STICK,N,gm.chan10.SIDE_STICK],
        continuant=True
    )
```

# Playing

Finally, playing a song. At this point, you should have an array of tracks. Pass that array to the `render()` method to finalize your song.

```
from beat import *
from n import *
from d import *
from c import *
from l import *
import gm.chan10, mm68, i

set_port("MM6")
set_ticks_per_beat(480)
set_beats_per_unit(4)
set_units_per_measure(1)
set_beats_per_minute(120)

tracks=[]
tracks.append(
	notes(
		(C4,D4,E4,F4,G4,A4,B4,C5),
		channel=1
	)
)
tracks.append(
    rhythm(
        "1u",4,
        pattern=[gm.chan10.ACOUSTIC_SNARE,gm.chan10.SIDE_STICK,N,gm.chan10.SIDE_STICK]
    )
)
tracks.append(
    rhythm(
        "2u",4,
        pattern=[gm.chan10.TAMBOURINE]
    )
)

render(tracks)
```

Finally, run this python script like so:

```
> song.py port
```

to play it over the specified midi port, or

```
> song.py file ./song.mid
```

to generate a midi file.
