from beat import *
from beat.n import *
from beat.d import *
from beat.c import *
from beat.l import *
from beat import gm, mm68, i

set_port("MM6")
set_ticks_per_beat(480)
set_beats_per_unit(4)
set_units_per_measure(1)
set_beats_per_minute(120)

tracks=[]
tracks.append(
	notes(
		# notes
		(C4,D4,E4,F4,G4,A4,B4,C5),
		# # dyads
		(m2(C4),M2(C4),m3(C4),M3(C4)),
		(P4(C4),TT(C4),P5(C4),m6(C4)),
		(M6(C4),m7(C4),M7(C4),P8(C4)),
		# # chords
		(C(C4),CM6(C4),C7(C4),CM7(C4)),
		(Cm(C4),Cm6(C4),Cm7(C4),CmM7(C4)),
		(Cd(C4),Cd7(C4),Chd7(C4),Cd(C4)+i.M7(C4)), # Cdim add7
		(P5(C4)+i.P4(C4),P5(C4)+i.M2(C4),P5(C4)+i.P4(C4)+i.M7(C4),P5(C4)+i.M2(C4)+i.M7(C4),), # C sus4, C sus2, C sus4 add7, C sus2 add7
		(C(C4),C(D4),C(E4)),
		(C(F4),C(G4),C(A4)),
		(C(B4),C(C5),C(D5),C(E5),C(F5)),
		channel=1
	)+
	# non-uniform note length
	notes((C4,D4|S2,E4,F4|S2,G4,A4|S2,B4|L2_1,C5,D5,E5,F5,G5,A5,B5,C6),"2u")
)

render(tracks)

# TODO make sure 5/4 time works with a 1u unit followed by a 1b unit
