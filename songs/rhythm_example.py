from beat import *
from beat.n import *
from beat import gm, mm68

set_port("MM6")
set_ticks_per_beat(480)
set_beats_per_unit(4)
set_units_per_measure(1.25)
set_beats_per_minute(120)

tracks=[]
tracks.append(rhythm("1u",16,channel=10,pattern=[gm.chan10.ACOUSTIC_SNARE,gm.chan10.SIDE_STICK,N,gm.chan10.SIDE_STICK]))
tracks.append(rhythm("2u",16,channel=10,pattern=[gm.chan10.TAMBOURINE]))

render(tracks)
