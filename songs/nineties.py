from beat import *
from beat.n import *
from beat import gm, mm68

set_port("MM6")
set_ticks_per_beat(480)
set_beats_per_unit(16)
set_units_per_measure(1)
set_beats_per_minute(200)

tracks=[]
tracks.append(rhythm("1u",16,channel=10,pattern=[gm.chan10.TAMBOURINE,gm.chan10.TAMBOURINE,gm.chan10.HAND_CLAP,N,gm.chan10.TAMBOURINE,gm.chan10.TAMBOURINE,gm.chan10.HAND_CLAP,N,gm.chan10.TAMBOURINE,gm.chan10.TAMBOURINE,gm.chan10.HAND_CLAP,gm.chan10.TAMBOURINE,gm.chan10.TAMBOURINE,gm.chan10.TAMBOURINE,gm.chan10.HAND_CLAP,N]))

render(tracks)
